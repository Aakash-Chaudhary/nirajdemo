<?php

namespace App\Models;
use App\Models\User;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingCar extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'vehicle_mode',
        'vehicle_number',
        'seater',
        'rent_per_day',
        'no_of_day',
        'starting_date'

    ];

    protected $dates = ['deleted_at'];


     /**
     * Get user
     *
     * @return User
     */
    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
