<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;


class CarProduct extends Model
{
    use SoftDeletes;

    use HasFactory, HasApiTokens;

    protected $fillable = [
        'agent_id',
        'vehicle_mode',
        'vehicle_number',
        'seater',
        'rent_per_day',
        'status',

    ];

    protected $dates = ['deleted_at'];
}
