<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CarProduct;
use DB;
use App\Models\BookingCar;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    //

    public function book_car(Request $req)
    {
        $user_id= auth()->user()->id;

        try{

            if(!empty($req->all()))
            {
                BookingCar::create(
                [
                'user_id'=>$user_id,
                'vehicle_mode'=>$req->vehicle_mode,
                'vehicle_number'=>$req->vehicle_number,
                'seater'=>$req->seater,
                'no_of_day'=>$req->days,
                'starting_date'=>$req->date,
                'rent_per_day'=>$req->rent_per_day

                ]);

                CarProduct::where('vehicle_number',$req->vehicle_number)->update([
                    'status'=>1
                ]);
                return redirect()->back()->with('success', 'Congratulation Your booking is confirm!');
            }
        }
        Catch(\Exception $e)
        {
            return redirect()->back()->with('error', 'Something went wrong, please try again!');
        }
    }

    public function user_history()
    {
            $products=BookingCar::where('user_id',auth()->user()->id)->orderBy('created_at','DESC')->get();
            return view('user_history')->with('product',$products);
    }

    public function cancel_booking($id)
    {
         try{

            $get_vehicle_number=BookingCar::where('id',$id)->get();
            CarProduct::where('vehicle_number',$get_vehicle_number[0]->vehicle_number)->update([
                'status'=>0
            ]);

             BookingCar::where('id',$id)->delete();

           return redirect()->back()->with('success', 'Successfully, Your booking is cancel!');
        }
        Catch(\Exception $e)
        {
            return redirect()->back()->with('error', 'Something went wrong, please try again!');

        }
    }

}
