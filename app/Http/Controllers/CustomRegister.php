<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use DB;
use Illuminate\Support\Facades\Hash;

class CustomRegister extends Controller
{
    //

    public function index()
    {
        return view('auth.customeregister');
    }
    public function store(Request $request)
    {
    //  dd($request->all());
        //dd($request->all());
        $request->validate(
            [

                'name' => 'required', 'string', 'max:255',
                'email' => 'required', 'string', 'email', 'max:255', 'unique:users',
                'phone' => 'required',  'max:12',
                'password' => 'required', 'string', 'min:8', 'confirmed',

            ]
        );

        if(!is_null($request->all()))
        {
                $data = User::create([
                'role_id'=>$request['role_id'],
                'name' => $request['name'],

                'email' => $request['email'],
                'phone' => $request['phone'],
                'password' => Hash::make($request['password']),
            ]);

            return redirect()->route('login')->with('success','You have successfully Registered as an Agent !!');
        }
        else
        {
            return back()->with('error', 'something went wrong!');

        }


    }
}
