<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CarProduct;
use DB;
use App\Models\BookingCar;
use Illuminate\Support\Facades\Auth;


class AgentController extends Controller
{
    //
    public function index()
    {
        $agent_id= auth()->user()->id;
        $products=CarProduct::orderBy('created_at','DESC')->where('agent_id',$agent_id)->get();

        return view('agentdashboard')->with('product',$products);
    }



    public function add_data_store(Request $req)
    {
        $agent_id= auth()->user()->id;

        try{

            if(!empty($req->all()))
            {
                CarProduct::create(
                [
                'agent_id'=>$agent_id,
                'vehicle_mode'=>$req->vehicle_mode,
                'vehicle_number'=>$req->vehicle_number,
                'seater'=>$req->seater,
                'status'=>0,
                'rent_per_day'=>$req->rent_per_day

                ]);
                return redirect()->back()->with('success', 'Successfully added product!');
            }
        }
        Catch(\Exception $e)
        {
            return redirect()->back()->with('error', 'Something went wrong, please try again!');
        }


    }

    public function update_product(Request $req)
    {
        try{

            if(!empty($req->all()))
            {
                CarProduct::where('id',$req->id2)->update(
                    [
                    'vehicle_mode'=>$req->vehicle_mode2,
                    'vehicle_number'=>$req->vehicle_number2,
                    'seater'=>$req->seater2,
                    'rent_per_day'=>$req->rent2
                    ]);
                return redirect()->back()->with('success', 'Successfully updated product!');
            }
        }
        Catch(\Exception $e)
        {
            return redirect()->back()->with('error', 'Something went wrong, please try again!');
        }
    }

    public function delete_product($id)
    {
        $data = CarProduct::where('id',$id)->delete();
        return redirect()->back();
    }

    public function view_product($id)
    {
        $data = CarProduct::where('id',$id)->get();
        $booking_data=BookingCar::where('vehicle_number',$data[0]->vehicle_number)->get();

        return view('view_product')->with('product',$booking_data);
    }

}
