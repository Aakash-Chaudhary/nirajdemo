<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingCarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {


        Schema::create('booking_cars', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->string('vehicle_mode');
            $table->string('vehicle_number')->unique();
            $table->integer('seater');
            $table->integer('rent_per_day');
            $table->integer('no_of_day');
            $table->string('starting_date');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking_cars');
    }
}
