<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomRegister;
use App\Http\Controllers\AgentController;
use App\Models\CarProduct;
use App\Models\BookingCar;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $products=CarProduct::where('status',0)->orderBy('created_at','DESC')->get();

    return view('welcome')->with('product',$products);
});

Auth::routes();

// *************************************** CustomRegister ***************************8

Route::get('/register_as_agent',[App\Http\Controllers\CustomRegister::class, 'index'])->name('register_as_agent');
Route::post('/customregister',[App\Http\Controllers\CustomRegister::class, 'store'])->name('customregister');

// ********************************************admin side AgentController **************************************

Route::get('/agentdashboard', [App\Http\Controllers\AgentController::class, 'index'])->name('agentdashboard')->middleware('agent');
Route::post('/add_data_store',[App\Http\Controllers\AgentController::class, 'add_data_store'])->name('add_data_store')->middleware('agent');
Route::post('/update_product',[App\Http\Controllers\AgentController::class, 'update_product'])->name('update_product')->middleware('agent');
Route::any('/delete_product/{id}',[App\Http\Controllers\AgentController::class, 'delete_product'])->name('delete_product')->middleware('agent');
Route::get('/view_product/{id}', [App\Http\Controllers\AgentController::class, 'view_product'])->name('view_product')->middleware('agent');

// *************************** User side ********************************************

Route::middleware(['customer', 'verified'])->get('/dashboard', function () {
    $products=CarProduct::where('status',0)->orderBy('created_at','DESC')->get();

    return view('dashboard')->with('product',$products);
})->name('dashboard');

Route::post('/book_car',[App\Http\Controllers\DashboardController::class, 'book_car'])->name('book_car')->middleware('customer');
Route::get('/user_history', [App\Http\Controllers\DashboardController::class, 'user_history'])->name('user_history')->middleware('customer');
Route::any('/cancel_booking/{id}',[App\Http\Controllers\DashboardController::class, 'cancel_booking'])->name('cancel_booking')->middleware('customer');
