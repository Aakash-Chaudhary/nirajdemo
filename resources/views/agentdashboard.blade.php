<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Agent Dashboard') }}
        </h2>
    </x-slot>

    @if(session('success'))
    <div class="alert alert-success">{{session('success')}}</div>
    @endif

    @if(session('error'))
        <div class="alert alert-danger">{{session('error')}}</div>
    @endif
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="container-fluid">
            <div class="d-flex">
                <button  type="button" data-bs-toggle="modal" data-bs-target="#exampleModal" class="btn btn-sucess" style="background:greenyellow">Add Car</button>
            </div>
        </div>
    </div>
</div>

<table class="table">
    <thead>
        <tr>
            <th class="col">S.No</th>
            <th class="col">Vehicle Model</th>
            <th class="col">Vehicle Number</th>
            <th style="display:none"></th>
            <th class="col">Seater</th>
            <th class="col">Rent Per/Day</th>
            <th class="col">Availability</th>
            <th class="col">Action</th>
        </tr>
    </thead>
    <tbody>
    <?php $count=1;?>
    @foreach($product as $products)
        <tr>
            <td>{{$count}}</td>
            <td class="vehicle_mode1">{{$products->vehicle_mode}}</td>
            <td class="vehicle_number1">{{$products->vehicle_number}}</td>
            <td class="id1"style="display:none">{{$products->id}}</td>
            <td class="seater1">{{$products->seater}}</td>
            <td class="rent1">{{$products->rent_per_day}}</td>
            <td>
                @if($products->status == 0)
                <span style="color:blue;"><h3><b>Available</b></h3></span>
                @else
                <span style="color:rgb(184, 40, 40);"><h3><b>Booked</b></h3></span>
                @endif
            </td>

            <td>
                @if($products->status == 1)
                <a href="{{url('view_product',$products->id)}}" class="btn btn-success mx-3">View</a>
                @else
                <a href="{{url('view_product',$products->id)}}" class="btn btn-success mx-3 disabled"><span class="learn-more" data-toggle="tooltip" data-placement="top"
                    title="This particular car has not booked yet"><i class="fas fa-info"></i></span>View</a>
                @endif
                <button data-bs-toggle="modal"  id="edit"  data-bs-target="#editModal" class="btn btn-info mx-3 edit">Edit</button>
                <a href="{{url('delete_product',$products->id)}}"id="delete"class="btn btn-danger mx-3" data-id="{{$products->id}}">Delete</a>
            </td>
        </tr>
    <?php $count++;?>
    @endforeach


    </tbody>
</table>

<!-- add- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Product</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
          <form action="{{route('add_data_store')}}" method="post" enctype="multipart/form-data">
              @csrf
                <div class="mb-3">
                    <label for="vehicle_model" class="form-label">Vehicle Model</label>
                    <input type="text" name="vehicle_mode" class="form-control" id="vehicle_mode" aria-describedby="emailHelp" required>
                </div>
                <div class="mb-3">
                    <label for="vehicle_model" class="form-label">Vehicle Number</label>
                    <input type="text" name="vehicle_number" class="form-control" id="vehicle_number" aria-describedby="emailHelp" required>
                </div>
                <div class="mb-3">
                    <label for="seater" class="form-label">Seater Type</label><br>
                    <select name="seater" class="form-select" id="seater">
                        <option value="2">2 seater</option>
                        <option value="4">4 seater</option>
                        <option value="6">6 seater</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="rent-per-day" class="form-label">Rent perDay</label>
                    <input type="text" name="rent_per_day" class="form-control" id="rent_per_day" aria-describedby="emailHelp" required>
                </div>

                {{-- <div class="mb-3">
                    <label for="rent-per-day" class="form-label">Attachment</label>
                    <input type="file"  name="image" class="form-control" id="image" aria-describedby="emailHelp">
                </div> --}}

                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
            </form>
      </div>
    </div>
  </div>
</div>

{{-- edit modal --}}

<div class="modal fade" class="editModal" id="editModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit product</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <form action="{{route('update_product')}}" method="post" encrypt="multipart/form-data">
                @csrf
                <input type="hidden" name="id2" id="id2"class="id2">
                  <div class="mb-3">
                      <label for="vehicle_model" class="form-label">Vehicle Model</label>
                      <input type="text" name="vehicle_mode2" class="form-control"  id="vehicle_mode2" aria-describedby="emailHelp">
                  </div>
                  <div class="mb-3">
                      <label for="vehicle_number" class="form-label">Vehicle Number</label>
                      <input type="text" name="vehicle_number2" class="form-control" id="vehicle_number2" aria-describedby="emailHelp">
                  </div>
                  <div class="mb-3">
                      <label for="seater" class="form-label">Seater Type</label><br>
                      <select name="seater2" class="form-select" id="seater2">
                          <option value="2">2 seater</option>
                          <option value="4">4 seater</option>
                          <option value="6">6 seater</option>
                      </select>
                  </div>
                  <div class="mb-3">
                      <label for="rent-per-day" class="form-label">Rent perDay</label>
                      <input type="text" name="rent2" class="form-control" id="rent2" aria-describedby="emailHelp">
                  </div>
                  <!-- <div class="mb-3">
                      <label for="rent-per-day" class="form-label">Attachment</label>
                      <input type="file"  name="image" class="form-control" id="image" aria-describedby="emailHelp">
                  </div> -->

                  <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                  <button type="submit" class="btn btn-primary">Update</button>
              </div>
              </form>
        </div>
      </div>
    </div>
  </div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <script>
      $(document).ready(function() {

      $(".edit").click(function(){

        var row = $(this).closest("tr");
        var vehicle_model_data = row.find('.vehicle_mode1').text();
        var vehicle_number_data = row.find('.vehicle_number1').text();
        var id_data = row.find('.id1').text();
        var seater_data = row.find('.seater1').text();
        var rent_data = row.find('.rent1').text();

        $("#editModal").on('shown.bs.modal', function(){

            $("#vehicle_mode2").val(vehicle_model_data);
            $("#vehicle_number2").val(vehicle_number_data);
            $("#seater2").val(seater_data);
            $("#id2").val(id_data);
            $("#rent2").val(rent_data);

        });

      });
    });
</script>


</x-app-layout>


