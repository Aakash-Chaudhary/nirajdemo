<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Agent Dashboard') }}
        </h2>
    </x-slot>

    @if(session('success'))
    <div class="alert alert-success">{{session('success')}}</div>
    @endif

    @if(session('error'))
        <div class="alert alert-danger">{{session('error')}}</div>
    @endif
    <div class="py-12">
        <a href="{{url('agentdashboard')}}" class="btn btn-danger mx-3">Dashboard</a>
    </div>



<table class="table">
    <thead>
        <tr>
            <th class="col">S.No</th>
            <th class="col">Vehicle Model</th>
            <th class="col">Vehicle Number</th>
            <th style="display:none"></th>
            <th class="col">Seater</th>
            <th class="col">Rent Per/Day</th>
            <th class="col">Booked By</th>
            <th class="col">Starting Date</th>
            <th class="col">No of Days</th>


        </tr>
    </thead>
    <tbody>
    <?php $count=1;?>
    @foreach($product as $products)
        <tr>
            <td>{{$count}}</td>
            <td class="vehicle_mode1">{{$products->vehicle_mode}}</td>
            <td class="vehicle_number1">{{$products->vehicle_number}}</td>
            <td class="id1"style="display:none">{{$products->id}}</td>
            <td class="seater1">{{$products->seater}}</td>
            <td class="rent1">{{$products->rent_per_day}}</td>
            <td class="username">{{$products->user->name}}</td>
            <td class="username">{{$products->starting_date}}</td>
            <td class="username">{{$products->no_of_day}}</td>


        </tr>
        <?php $count++;?>
    @endforeach


    </tbody>
</table>



</x-app-layout>



