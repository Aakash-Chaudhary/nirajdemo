<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('User Dashboard') }}
        </h2>
    </x-slot>

    @if(session('success'))
    <div class="alert alert-success">{{session('success')}}</div>
    @endif

    @if(session('error'))
        <div class="alert alert-danger">{{session('error')}}</div>
    @endif
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        <div class="container-fluid">
            <div class="d-flex">
                <a href="{{url('user_history')}}" type="button"  class="btn btn-sucess" style="background:rgb(139, 224, 11)">Check Booking</a>
            </div>
        </div>
    </div>
</div>

<table class="table">
    <thead>
        <tr>
            <th class="col">S.No</th>
            <th class="col">Vehicle Model</th>
            <th class="col">Vehicle Number</th>
            <th style="display:none"></th>
            <th class="col">Seater</th>
            <th class="col">Rent Per/Day</th>
            <th class="col">Action</th>
        </tr>
    </thead>
    <tbody>
    <?php $count=1;?>
    @foreach($product as $products)
        <tr>
            <td>{{$count}}</td>
            <td class="vehicle_mode1">{{$products->vehicle_mode}}</td>
            <td class="vehicle_number1">{{$products->vehicle_number}}</td>
            <td class="id1"style="display:none">{{$products->id}}</td>
            <td class="seater1">{{$products->seater}}</td>
            <td class="rent1">{{$products->rent_per_day}}</td>
            <td>
                <button data-bs-toggle="modal"  id="rent"  data-bs-target="#exampleModal" class="btn btn-primary mx-3 rent">Rent Car</button>
            </td>
        </tr>
        <?php $count++;?>
    @endforeach


    </tbody>
</table>

<!-- add- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Book a Car</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
          <form action="{{route('book_car')}}" method="post" enctype="multipart/form-data">
              @csrf
              <input type="hidden" class="id2" id="id2">
                <div class="mb-3">
                    <label for="vehicle_model" class="form-label">Vehicle Model</label>
                    <input type="text" name="vehicle_mode" class="form-control" id="vehicle_mode2" aria-describedby="emailHelp" readonly>
                </div>
                <div class="mb-3">
                    <label for="vehicle_model" class="form-label">Vehicle Number</label>
                    <input type="text" name="vehicle_number" class="form-control" id="vehicle_number2" aria-describedby="emailHelp" readonly>
                </div>
                <div class="mb-3">
                    <label for="seater" class="form-label">Seater Type</label><br>
                    <input type="text" name="seater" class="form-control" id="seater2" aria-describedby="emailHelp" readonly>

                </div>
                <div class="mb-3">
                    <label for="rent-per-day" class="form-label">Rent per/Day</label>
                    <input type="text" name="rent_per_day" class="form-control" id="rent2" aria-describedby="emailHelp" readonly>
                </div>

                <div class="mb-3">
                    <label for="Rent Date" class="form-label">Rent Date</label>
                    <input type="date" name="date" class="form-control" id="date" aria-describedby="emailHelp" required>
                </div>

                <div class="mb-3">
                    <label for="Rent Date" class="form-label">No of Rented Days</label>
                    <input type="number" name="days" class="form-control" id="days" aria-describedby="emailHelp" required>
                </div>

                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Rent</button>
            </div>
            </form>
      </div>
    </div>
  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <script>
    $(document).ready(function()
    {

        $(".rent").click(function()
        {
            // alert("hii");

            var row = $(this).closest("tr");
            var vehicle_model_data = row.find('.vehicle_mode1').text();
            var vehicle_number_data = row.find('.vehicle_number1').text();
            var id_data = row.find('.id1').text();
            var seater_data = row.find('.seater1').text();
            var rent_data = row.find('.rent1').text();


            $("#exampleModal").on('shown.bs.modal', function()
            {
                $("#vehicle_mode2").val(vehicle_model_data);
                $("#vehicle_number2").val(vehicle_number_data);
                $("#seater2").val(seater_data);
                $("#id2").val(id_data);
                $("#rent2").val(rent_data);
            });

        });
    });
</script>



</x-app-layout>



